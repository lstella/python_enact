This repository contains the material for the

Python workshop of the ENACT project
------------------------------------

held in Belfast on 8-9 June 2016.

The tutorials are based on `Ipython` (AKA `Jupyter`) notebooks, which can be found in the 
directory Notebooks/.


To run the notebooks, move to the Notebooks/ directory and type

jupyter notebook


If you don't have a `python` distribution installed, yet, you can download and 
install the free version of `Anaconda` from

[Anaconda 2.7](https://www.continuum.io/downloads).

