{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n",
    "\n",
    "By completing this notebook, you will we able to:\n",
    "- Create a `numpy` array.\n",
    "- Define a `python` function.\n",
    "- Plot a simple function with `matplotlib`.\n",
    "- Write and read formatted data files."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `python` ecosystem is very rich and we are going to use just a few libraries. The most important are `numpy` and `matplotlib`. This libraries are imported by means of the follwing commands: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "# The following line is meant for the ipython notebook, only\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The name and behaviour of many `numpy` built-in functions is similar to the corresponding `MATLAB` functions. For instance, we can use the function `linspace` creates an array of twenty regularly spaced points between `0` and `1`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "baseline = np.linspace(0., 1., 20)\n",
    "print baseline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that *both* extremes are included. \n",
    "\n",
    "The built in function `help` is used to retrieve more information about any `python` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "help(np.linspace)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Basic operations on `numpy` arrays are *vectorised*, i.e., no `for` loop on the array's indices is required."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "baseline2 = baseline**2 # We take the square of the baseline\n",
    "print baseline2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `numpy` function `power` does the same."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "baseline2 = np.power(baseline, 2)\n",
    "print baseline2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now ready to make our first plot with `matplotlib`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.plot(baseline, baseline2,'r-') # The option 'r-' set the red line style \n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`matplotlib` allows the user to customise almost every detail of a plot. Have a look at the [this gallery](http://matplotlib.org/gallery.html) for a few samples. Also note that no plot is shown if the command `plt.show()` is absent or commented out (with the `#` key). \n",
    "\n",
    "Title, labels (`latex` formatting included!), and line formating are easily added."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.plot(baseline, baseline2,'b--') # The option 'b---' set the blue dashed line style \n",
    "plt.title('A parabola')\n",
    "plt.xlabel('$x$')\n",
    "plt.ylabel('$x^2$')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can allow more freedom to our parabola and define a function which depends on a few parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def parabola(x, pars = None):\n",
    "    \"\"\"Return the values of a parabola\n",
    "    \n",
    "        Parameters\n",
    "        ----------\n",
    "        x : ndarray\n",
    "            The x-coordinates of the parabola.\n",
    "        pars : ndarray\n",
    "            The coefficients of the parabola.\n",
    "            The index correspons to the order of the coefficient, \n",
    "            e.g., pars[2] gives the coefficient of the quadratic term.\n",
    "            Default is pars = [0., 0., 1.].\n",
    "            \n",
    "        Returns\n",
    "        -------\n",
    "        y : ndarray\n",
    "            The y-coordinates of the parabola.\"\"\"\n",
    "    \n",
    "    # By default, it is a simple parabola\n",
    "    if pars is None: \n",
    "        pars = np.array([0., 0., 1.])\n",
    "    \n",
    "    # make sure we can manipulate pars as a numpy array\n",
    "    try:\n",
    "        pars = np.array(pars, dtype=float)\n",
    "    except:\n",
    "        print \"ERROR: the parameters are not in the expected format!\"\n",
    "    \n",
    "    # make sure there are at least three parameters\n",
    "    npad = np.maximum(0,3-pars.size)\n",
    "    pars = np.pad(pars.ravel(),(npad,0),'constant') # zero padding   \n",
    "    \n",
    "    return pars[2]*x**2+pars[1]*x+pars[0] # Note that python is zero-based, i.e., the first index is 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Don't worry if the syntax is not clear, yet! This example is just meant elicit your curiosity and show what this function is really meant for, e.g., check for consistency. The *mathematical* operation itself is rather straighforward, but checking for the consistency of the input variables is not. `python` offers many shortcuts and even the possibility to set default parameters.\n",
    "\n",
    "Note the *docstring* right after the definition. As for the built-in function, the docstring is prompted by the function `help`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "help(parabola)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before delving into details, let's explore the behaviour of this function by plotting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "baseline = np.linspace(-1., 1., 100) # We have changed the baseline\n",
    "pars = [0., 1., 1.]\n",
    "plt.plot(baseline, parabola(baseline, pars),'r-')\n",
    "plt.title('A parabola')\n",
    "plt.xlabel('$x$')\n",
    "plt.ylabel('$y=f(x)$')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Class exercise\n",
    "\n",
    "- Try to change the values or the length of the list `pars`. Even try to comment out the line! \n",
    "- Add a warning when the coefficient of the quadratic term is zero."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point, you may want to save our results on disk for future reuse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "fp = open('parabola.dat','w')\n",
    "fp.write('# This file contains the raw data to plot a parabola\\n')\n",
    "for x, y in zip(baseline, parabola(baseline, pars)):\n",
    "    fp.write('  %12.7f  %12.7f\\n'%(x,y)) # string formatting like in C/C++\n",
    "fp.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we have used a `for` loop here, but not over an array's index. In `python` one can loop directly on the elements of an array (or any other *iterable* data container). As we have two arrays of the same shape (or dimension), we can use a very useful trick: the two arrays are *zipped* together (using the built-in function `zip`) and the loop can run simultanously on two elements, i.e., the *tuple* `(x, y)`.\n",
    "\n",
    "We can edit the file `parabola.dat` and check whether everything is fine. Alternatively, we can load the file and plot it. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data = np.loadtxt('parabola.dat')\n",
    "plt.plot(data[:,0], data[:,1],'r-')\n",
    "plt.title('A parabola')\n",
    "plt.xlabel('$x$')\n",
    "plt.ylabel('$y=f(x)$')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`numpy` can easily read well-formatted files like our `parabola.dat`. By default, the comment lines starting with `#` are skipped, and the rest of the file is stored into the array `data`. This is a multidimensional array, as we can check by means of the array method `shape`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print data.shape # Tuple containing the array's dimensions\n",
    "print data.size # Total number of elements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The array `data` can be then *sliced* to plot the second column, `data[:,1]`, against the first one, `data[:,0]` (remember that `python` is zero-based). We can also check that they have the same dimension:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print data[:,0].shape\n",
    "print data[:,1].shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we briefly discuss the `python` performance. Let's consider the average execution time of function `parabola`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%timeit parabola(baseline, pars)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now consider the average execution time of an *alternative version* which makes explicit use of a `python` loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def parabola_alt(x, pars = None):\n",
    "    \"\"\"Return the values of a parabola\n",
    "    \n",
    "        Parameters\n",
    "        ----------\n",
    "        x : ndarray\n",
    "            The x-coordinates of the parabola.\n",
    "        pars : ndarray\n",
    "            The coefficients of the parabola.\n",
    "            The index correspons to the order of the coefficient, \n",
    "            e.g., pars[2] gives the coefficient of the quadratic term.\n",
    "            Default is pars = [0., 0., 1.].\n",
    "            \n",
    "        Returns\n",
    "        -------\n",
    "        y : ndarray\n",
    "            The y-coordinates of the parabola.\"\"\"\n",
    "    \n",
    "    # By default, it is a simple parabola\n",
    "    if pars is None: \n",
    "        pars = np.array([0., 0., 1.])\n",
    "    \n",
    "    # Make sure we can manipulate pars as a numpy array\n",
    "    try:\n",
    "        pars = np.array(pars, dtype=float)\n",
    "    except:\n",
    "        print \"ERROR: the parameters are not in the expected format!\"\n",
    "    \n",
    "    # Make sure there are at least three parameters\n",
    "    npad = np.maximum(0,3-pars.size)\n",
    "    pars = np.pad(pars.ravel(),(npad,0),'constant') # zero padding\n",
    "    \n",
    "    # Initialise to zero \n",
    "    y = np.zeros_like(x)\n",
    "    \n",
    "    for index in range(y.shape[0]):\n",
    "        y[index] = pars[2]*x[index]**2+pars[1]*x[index]+pars[0]\n",
    "        \n",
    "    return y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both version give the same plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "baseline = np.linspace(-1., 1., 100) # We have changed the baseline\n",
    "pars = [0., 1., 1.]\n",
    "plt.plot(baseline, parabola_alt(baseline, pars),'r-')\n",
    "plt.title('A parabola')\n",
    "plt.xlabel('$x$')\n",
    "plt.ylabel('$y=f(x)$')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but the alternative version takes roughly twice the time of the \"loop-less\" version to execute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%timeit parabola_alt(baseline, pars)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This simple example suggests that `for` loops should be avoided and vectorised `numpy` operations preferred whenever it's possible.\n",
    "\n",
    "In practise, `python` is great for productivity and prototyping. When its numerical performance (or under-performance) becomes an issue, it's still possible to re-write some criticcaly slow part of the code in `cython`. The interested reader can find more information [here](http://cython.org/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
