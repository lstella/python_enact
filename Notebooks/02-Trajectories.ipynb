{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Looking at MD trajectories\n",
    "\n",
    "By completing this notebook, you will we able to:\n",
    "- Parse an xyz file.\n",
    "- Use `numpy` arrays to store and manipulate the atomic coordinates.\n",
    "- Use `python` list comprehension.\n",
    "- Use `numpy` array broadcasting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are many pieces of software that one can use to visualise and manipulate a MD trajectories, e.g., [Avogadro](http://avogadro.cc). However complete they are, there is always something different we want to compute and visualise. In the following, we will see how to get the job quickly done with `python`. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we need to import the atomic coordinates and we assumed they are all stored in the so-called [\"xyz\" format](https://en.wikipedia.org/wiki/XYZ_file_format). In particular, we have an instance of our favourite molecule in the file `caffeine.xyz`.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.constants import physical_constants\n",
    "import matplotlib.pyplot as plt\n",
    "# The following line is meant for the ipython notebook, only\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As usual, we start importing some relevant modules (e.g., `numpy`) or functions (e.g., `physical_constants`) we will use later. There is no need to fully import a large library (e.g., `scipy`) and `python` let us import just what's needed to the task.\n",
    "\n",
    "The function `read_frame` below is far from being perfect (e.g., it does not check the file format and it reads just one frame),"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def read_frame(fname):\n",
    "    \"\"\"Parse a frame of an xyz file\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    fname : string\n",
    "        The name of the xyz file.\n",
    "             \n",
    "    Returns\n",
    "    -------\n",
    "    names : list\n",
    "        The names of the atoms.\n",
    "    pos : ndarray\n",
    "        The coordinates of the atoms.\"\"\"    \n",
    "    \n",
    "    try:\n",
    "        fp = open(fname, 'r')\n",
    "    except:\n",
    "        print \"ERROR: the line %s cannot be open!\"%(fname)\n",
    "\n",
    "    # Set the number of atoms    \n",
    "    natom = int(fp.readline().strip())\n",
    "    \n",
    "    # skip the comment line\n",
    "    fp.readline()\n",
    "\n",
    "    names = []\n",
    "\n",
    "    pos = []\n",
    "\n",
    "    for atom in range(natom):\n",
    "        line = fp.readline().strip().split()\n",
    "\n",
    "        names.append(line[0])\n",
    "\n",
    "        pos.append(line[1:4])\n",
    "        \n",
    "    fp.close()    \n",
    "\n",
    "    pos = np.array(pos, dtype = float)\n",
    "\n",
    "    return names, pos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but it does the job. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "fname = '../Data/caffeine.xyz'\n",
    "names, pos = read_frame(fname)\n",
    "print names\n",
    "print pos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Although not central to the task, one can think of sorting the atomic coordinates according to the atom names. `numpy` provides a few functions to sort and searching, see [here](http://docs.scipy.org/doc/numpy/reference/routines.sort.html).\n",
    "\n",
    "Perhaps the safer way is to sort the atomic coordinates \"virtually\" by using the indices of the sorted names (i.e., the original containers are *not* modified)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ind = np.argsort(names) # Returns the indices of the sorter list\n",
    "print ind\n",
    "print [names[ii] for ii in ind] # This is an example of \"list comprehension\"\n",
    "print pos[ind] # Indexing is easier with numpy arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One may also want to know the coordinates of nitrogen atoms, only."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "is_N = [name == 'N' for name in names] # This is another example of \"list comprehension\"\n",
    "print pos[np.where(is_N)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Knowing the name of the atoms is usefull, because some properties (e.g., the atomic mass) can be inferred from the atom names. To this end, we need to use another `python` container called `dictionary`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "mass = {'H': 1.00784, 'C': 12.0096, 'N': 14.00643, 'O': 15.99903} # Source: NIST. Unit: AMU.\n",
    "print mass\n",
    "print mass.keys()\n",
    "print mass.values()\n",
    "print mass['N']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In fact this dictionary should be longer, but this is what we need at the moment. Note that dictionaries are *not* ordered, but they are *hashable*, i.e., their keys are uniquely defined. \n",
    "\n",
    "If you don't remember the value of the AMU, `scipy` comes to the rescue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "AMU = physical_constants['atomic mass constant']\n",
    "print \"AMU = %g %s\"%(AMU[0], AMU[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now create a `numpy` array with the masses of our atoms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "masses = np.array([mass[name] for name in names])\n",
    "print masses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use this array to compute the centre of mass of our favourite molecule:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "com = np.sum(pos*masses[:, None], axis=0)/np.sum(masses) # Here we have 'broadcasted' an array product\n",
    "print com"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this simple task, we used the full power of the `numpy` arrays, namely:\n",
    "- The *element-wise* product of the two arrays `pos` and `masses` have been broadcasted. [Broadcast rules](http://docs.scipy.org/doc/numpy-1.10.1/user/basics.broadcasting.html) are used by the `python` interpreter to set the default behaviour of an array operation between arrays of *different* shape. By default, the *last* dimension of the two arrays must agree. As this is not the case for our centre of mass calculation, we had to tell the interpreter how to deal with the `masses` array.\n",
    "- The *element-wise* sum of a multidimensional array can be taken with respect to *any* index (or axis). Since the default is again the last index (or axis), we had to tell the interpreter to use the *first* index, instead. In fact, this is the second-to-last index."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "com = np.sum(pos*masses[:, None], axis=-2)/np.sum(masses) # -2 means second-to-last index\n",
    "print com"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As there are many thinks going on behind the scenes, it is a good idea to double-check the value of the centre of mass. For instance, we can re-compute the centre of mass after an appropriate translation of the atomic coordinates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print np.sum((pos-com)*masses[:, None], axis=0)/np.sum(masses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now write a function to read all the frames at once."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def read_frames(fname):\n",
    "    \"\"\"Parse an xyz file\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    fname : string\n",
    "        The name of the xyz file.\n",
    "             \n",
    "    Returns\n",
    "    -------\n",
    "    names : list\n",
    "        The names of the atoms.\n",
    "    pos : ndarray\n",
    "        The coordinates of the atoms.\"\"\"    \n",
    "    \n",
    "    try:\n",
    "        fp = open(fname, 'r')\n",
    "    except:\n",
    "        print \"ERROR: the file %s cannot be open!\"%(fname)\n",
    "        \n",
    "    # Read all lines    \n",
    "    data = fp.readlines()\n",
    "    \n",
    "    fp.close()\n",
    "\n",
    "    # Set the number of atoms\n",
    "    natom = int(data[0].strip())\n",
    "\n",
    "    # Filter the lines with the atomic coordinates\n",
    "    atoms = [data[ii].strip().split() for ii in range(len(data)) if not (ii%(natom+2) == 0 or ii%(natom+2) == 1)] \n",
    "\n",
    "    names = []\n",
    "\n",
    "    pos = []\n",
    "    \n",
    "    for atom in atoms:\n",
    "        names.append(atom[0])\n",
    "\n",
    "        pos.append(atom[1:4])\n",
    "\n",
    "    names = names[:natom] # By default the first index is 0\n",
    "\n",
    "    # The shape of the pos array is: (number of frames, number of atoms, number of coordinates)\n",
    "    pos = np.array(pos, dtype = float).reshape((-1, natom, 3)) # -1 means that the dimension is inferred\n",
    "\n",
    "    return names, pos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function is called as before, although it returna a larger array with the atomic coordinates of *all* frames. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "fname = '../Data/caffeine.xyz'\n",
    "names, pos = read_frames(fname)\n",
    "print names\n",
    "print pos.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Amazingly, we can use the same line of code to compute the centre of mass of *all* the frames at once."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "com = np.sum(pos*masses[:, None], axis=-2)/np.sum(masses) # -2 means second-to-last index\n",
    "print com.shape\n",
    "print com[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Class exercise\n",
    "\n",
    "- Plot the centre of mass against the frame number. Have all three coordinates in a single plot and set labels etc. accordingly.\n",
    "- Include a few format checks in the function `read_frames`, e.g., does the first line of the frame contain just one (non-negative) integer? Is the total number of lines exactly divisible by the number of frames? Are the atom names always the same and always in the same order? \n",
    "- In the file `caffeine.xyz` an \"extended\" xyz format is used so that the atomic velocities are also reported. Extend the function `read_frames` to return an array with the velocities, too. Estimate the \"instantaneous temperature\" of the molecule from its instantaneous kinetic energy.\n",
    "- Compute and plot the angular momentum of the molecule for each frame.\n",
    "- Compute the moment of inertia (or inertia tensor) of the molecule for each frame. See [here](http://mathworld.wolfram.com/MomentofInertia.html) for the a definition of the inertia tensor."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
