
#  Copyright (C) 2016 by Lorenzo Stella <lorenzo DOT stella77 AT gmail.com>

#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:

#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.

#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.

import numpy as np
from sys import exit
from scipy.constants import physical_constants
from scipy.spatial.distance import pdist, squareform

# Constants
BOHR = physical_constants["Bohr radius"][0]/1.e-10 # A

print " # BOHR      = %g [A]"%(BOHR)

HBAR = physical_constants["Planck constant over 2 pi in eV s"][0]/1.e-15 # eV *fs

print " # HBAR      = %g [eV*fs]"%(HBAR)

AMU = physical_constants["atomic mass unit-electron volt relationship"][0]\
/(physical_constants["speed of light in vacuum"][0]**2)*1.e10 # eV *fs^2 /A^2 

print " # AMU       = %g [eV*fs^2/A^2]"%(AMU)

KB = physical_constants["Boltzmann constant in eV/K"][0] # eV /K

print " # KB        = %g [eV/K]"%(KB)

EPS = np.finfo(np.float32).eps

print " # EPS       = %g []"%(EPS)

EPS2 = np.finfo(np.float64).eps

print " # EPS2      = %g []"%(EPS2)

skip = int(1.e2)

# Mass dictionary. Source: NIST. Unit: AMU. BUGFIX: incomplete
mass = {'H': 1.00784, 
        'C': 12.0096, 
        'N': 14.00643, 
        'O': 15.99903, 
       'Ar': 39.948} 

# Lennard-Jones parameters. BUGFIX: argon, only
epsilon = 119.8 *KB # unit: eV

sigma = 3.405 # unit: A

class cluster(object):
    np.random.seed(seed=12345)

    def __init__(self, fname):
        self.names, self.pos = self.read_frame(fname)

        self.masses = np.array([mass[name] for name in self.names])*AMU # Unit: eV *fs^2 /A^2 

        self.count = 0

        self.time = 0. # unit: fs

        self.dt = 1. # unit: fs

        self.Troom = 60. # unit: K

        # Thermal initial momenta
        self.mom = np.random.standard_normal(size=self.pos.shape)\
        *np.sqrt(KB*self.Troom*self.masses)[:, None] # unit: eV*s/A

        # To get rid of the CoM translation
        self.mom -= np.average(self.mom, axis=0) # unit: eV*s/A

    def read_frame(self, fname):
        """Parse a frame of an xyz file
    
        Parameters
        ----------
        fname : string
            The name of the xyz file.
             
        Returns
        -------
        names : list
            The names of the atoms.
        pos : ndarray
            The coordinates of the atoms."""    
    
        try:
            fp = open(fname, 'r')
        except:
            print "ERROR: the line %s cannot be open!"%(fname)

        # Set the number of atoms    
        natom = int(fp.readline().strip())
    
        # skip the comment line
        fp.readline()

        names = []

        pos = []

        for atom in range(natom):
            line = fp.readline().strip().split()

            names.append(line[0])

            pos.append(line[1:4])
        
        fp.close()    

        pos = np.array(pos, dtype = float)

        return names, pos

    def print_nrg(self, fp):
        ekin = 0.5*np.sum((np.linalg.norm(self.mom, axis=1)**2)/self.masses)

        epot = vv_lj(self.pos)

        fp.write(("  %12d")%(self.count)+("  %12.5e"*4)%(self.time, ekin, epot, ekin+epot)+"\n")

    def print_ang(self, fp):
        CoM = np.sum(self.pos*self.masses[:, None], axis=0)/np.sum(self.masses)

        angular = np.sum(np.cross(self.pos-CoM, self.mom, axis=1), axis=0)

        fp.write("  %12d  %12.5e"%(self.count, self.time))

        for el in angular: fp.write("  %12.5e"%(el))

        fp.write("\n")

    def print_xyz(self, fp):
        fp.write("%9d\n"%(self.pos.shape[0]))

        CoM = np.sum(self.pos*self.masses[:, None], axis=0)/np.sum(self.masses)

        fp.write("  step =  %12d; time =  %12.5e fs\n"%(self.count, self.time))

        for name, atom in zip(self.names, self.pos):
            fp.write("%6s"%(name))

            for el in atom: fp.write("  %12.5e"%(el))

            fp.write("\n")

    def position_step(self, dt):
        self.pos += (self.mom/self.masses[:, None])*dt

    def momentum_step(self, dt):
        force = np.zeros_like(self.pos)

        force += ff_lj(self.pos)

        self.mom += force*dt

    def propagate(self, nsteps):
        fp = open("lj.xyz", "w")

        fpe = open("lj_nrg.dat", "w")

        fpa = open("lj_ang.dat", "w")

        self.print_xyz(fp)

        self.print_nrg(fpe)

        self.print_ang(fpa)

        for step in range(nsteps):
            dt = self.dt

            self.count += 1
            
            self.time += dt
            
            # Half position_step
            self.position_step(0.5*dt)

            # Whole momentum_step
            self.momentum_step(dt)

            # Half position_step
            self.position_step(0.5*dt)

            if self.count % int(skip) == 0:
                self.print_xyz(fp)

                self.print_nrg(fpe)

                self.print_ang(fpa)

        fpa.close()

        fpe.close()

        fp.close()

def vv_lj(pos):
    rr = squareform(pdist(pos)) # unit: A

    epot = 0. # unit: eV

    for ia in range(pos.shape[0]):
        for ja in range(ia+1, pos.shape[0]):
            rij = rr[ia, ja] # unit: A

            epot += 4.*epsilon*((sigma/rij)**12-(sigma/rij)**6) # unit: A

    return epot        

def ff_lj(pos):
    rr = squareform(pdist(pos)) # unit: A

    force = np.zeros_like(pos) # unit: eV/A 

    for ia in range(pos.shape[0]):
        for ja in range(ia+1,pos.shape[0]):
            rij = rr[ia, ja] # unit: A

            dummy = -24.*epsilon*(2.*(sigma/rij)**12-(sigma/rij)**6)*(pos[ia]-pos[ja])/(rij**2) # unit: eV/A 

            force[ia] -= dummy # unit: eV/A 

            force[ja] += dummy # unit: eV/A 

    return force            

def main():
    nsteps = int(1.e5)

    lj5_mag = cluster("../Data/lj-5-opt.xyz")

    lj5_mag.propagate(nsteps)

if __name__ == "__main__":
    main()

